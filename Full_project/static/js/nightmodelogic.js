function night(night) {
    //let nightmode_color = document.getElementsByClassName('section nightmode_changed_color')[0];
    let col1 = document.getElementsByClassName('col-9')[0]
    let col2 = document.getElementsByClassName('card')[0]
    let nightmode_color = document.querySelectorAll("div[class^=nightmode_changed_color]");
    nightmode_color.forEach(element => {
        if (night) {
            element.style.background = "linear-gradient(to bottom, #0F2027, #203A43, #2C5364)";
            if(col1!=undefined){
                col1.style.background = "linear-gradient(to bottom, #0F2027, #203A43, #2C5364)";
                col2.style.background = "linear-gradient(to bottom, #0F2027, #203A43, #2C5364)";
            }
        }
        else {
            element.style.background = "linear-gradient(to bottom, #bdc3c7, #2c3e50)";
            if(col1!=undefined){
                col1.style.background = "linear-gradient(to bottom, #bdc3c7, #2c3e50)";
                col2.style.background = "linear-gradient(to bottom, #bdc3c7, #2c3e50)";
            }
        }
    });

}

let indicator = document.getElementsByClassName("indicator")[0];

let nightmode = $("#switcher").val();
nightmode = nightmode == "true" || nightmode == "True"
// console.log(nightmode);
function moveslider(mode) {

    if (mode) {
        indicator.style.transform = 'translate3d(25%, 0, 0)';
    } else {
        indicator.style.transform = 'translate3d(-75%, 0, 0)';
    }
}
moveslider(nightmode);
night(nightmode);

$("#switcher").click(function () {
    let nightmode = $(this).val();
    $.ajax({
        type: "GET",
        url: '/ajax/nightmode/',
        data: {
            'nightmode': nightmode
        },
        dataType: 'json',
        success: function (data) {
            // $("input").attr("value") = data.nightmode;
            $("#switcher").attr("value", data.nightmode);
            moveslider(data.nightmode);
            night(data.nightmode);
            //$("input").val("New text");


        }
    });

});

$("#nightmode-href").click(function(){
    $("#switcher").click()
})