$("#submit-create").click(function (event){
    event.preventDefault();
    $.ajax({
        type: "POST",
        url: "/ajax/createchat/",
        data: $("#createchatform").serializeArray(),
        success: function (data){
            window.location.replace(data.url)
        }
    });
});