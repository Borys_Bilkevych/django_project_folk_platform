$('document').ready(function(){
    const chatSocket = new WebSocket("wss://"+window.location.host+"/ws/chat/"+id+"/");

    chatSocket.onmessage = function(e) {
        const data = JSON.parse(e.data);
        addNewMessage(data.message, data.author_name);
        scrollDown();
    }

    chatSocket.onclose = function(e) {
        console.error("Chat socket closed unexpectedly");
    }

    $("#sendmessage").click(function () {
        let message = $("#id_message").val();
        if (message == ""){return}
        document.getElementById("id_message").value = "";
        $.ajax({
            type: "POST",
            url: '/ajax/send_message/',
            data: {
                'message': message,
                'chat_id': id
            },
            dataType: 'json',
            success: function (data) {
                let jsondata = JSON.stringify(data)
                chatSocket.send(jsondata)
            }
        });
    });

    $('#id_message').keypress(function(e){
        if(e.which == 13){
            $("#sendmessage").click();
        }
    });
});  


function addNewMessage(message, author){
    let messageBody= document.getElementById('board');

    let div = document.createElement("div");
    div.classList.add("card-panel");

    let div2 = document.createElement('div');
    div2.setAttribute('style', 'position: absolute; top: 0; left:3px; font-weight: bolder ' )
    div2.classList.add("title");

    div.innerHTML= message

    if(author != user_name){
        div.setAttribute("style", "width: 75%; position: relative; float: left;");
        div2.innerHTML = author
    }else{
        div.setAttribute("style", "width: 75%; position: relative; float: right;");
        div2.innerHTML = 'You'
    }
    div.append(div2);
    messageBody.append(div);
}

function scrollDown() {
    var elem = document.getElementById('board');
    elem.scrollTop = elem.scrollHeight;
} 