$('document').ready(function(){
    const commentsSocket = new WebSocket("wss://"+window.location.host+"/ws/comments/");

    commentsSocket.onmessage = function(e) {
        const data = JSON.parse(e.data);
        console.log('sdfsd')
        addNewComment(data.message, data.author);
        scrollDown();
    }

    commentsSocket.onclose = function(e) {
        console.error("Chat socket closed unexpectedly");
    }
    
    $("#send-comment").click(function (event){
        event.preventDefault();
        console.log("click")
        if(!$("#id_message").val().length){
            return
        }
        $.ajax({
            type: "POST",
            url: "/ajax/send_comment/",
            data: {"comment":$("#id_message").val()},
            success: function (data){
                console.log(data.guest)
                if(typeof data.guest === undefined){
                    alert("Guests are not allowed to leave comments!")
                    document.getElementById("id_message").value = "";
                    return
                }
                document.getElementById("id_message").value = "";
                let jsondata = JSON.stringify(data)
                commentsSocket.send(jsondata)
            }
        });
    });

    $('#send-comment').keypress(function(e){
        if(e.which == 13){
            $("#send-comment").click();
        }
    });
});  


function addNewComment(message, author){

    let commentBody= document.getElementById('card_comments');

    let div = document.createElement("div");
    div.classList.add("container");

    let img = document.createElement('img');
    img.setAttribute('src', 'https://st2.depositphotos.com/1010550/7946/i/600/depositphotos_79461036-stock-photo-standing-boys-silhouette.jpg' )
    img.setAttribute('alt', 'Avatar' )
    img.setAttribute('style', 'width:90px' )

    let p = document.createElement('p');
    p.innerHTML=author;
    let p1 = document.createElement('p');
    p1.innerHTML=message;
    div.append(img);
    div.append(p);
    div.append(p1);

    commentBody.append(div);
}

function scrollDown() {
    var elem = document.getElementById('card_comments');
    elem.scrollTop = elem.scrollHeight;
}
