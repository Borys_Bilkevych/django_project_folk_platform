$('document').ready(function(){
    // console.log("1")
    $("#click_button").click(function () {
        $.ajax({
            type: "GET",
            url: '/ajax/invite_user/',
            data: {
                'chat_id': id
            },
            dataType: 'json',
            success: function (data) {
                url = data.host+"/join/"+data.url+"/"
                copied(url)
                console.log(url)
            }
        });
    });
})


function copied(data) {
    var tooltip = document.getElementById("myTooltip");
    tooltip.innerHTML = "Copied!";
    textToClipboard(data)
}

function textToClipboard (text) {
    var dummy = document.createElement("textarea");
    document.body.appendChild(dummy);
    dummy.value = text;
    dummy.select();
    document.execCommand("copy");
    document.body.removeChild(dummy);
}

function outFunc() {
    var tooltip = document.getElementById("myTooltip");
    tooltip.innerHTML = "Generate Invite Link";
}
