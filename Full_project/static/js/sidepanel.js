function openNav() {
    let isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;
    let isTablet = window.matchMedia("only screen and (max-width: 960px)").matches;

    if (isMobile) {
        document.getElementById("mySidenav").style.width = "100%";
    }else if(isTablet){
        document.getElementById("mySidenav").style.width = "50%";
    }else{
        document.getElementById("mySidenav").style.width = "25%";
    }
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}