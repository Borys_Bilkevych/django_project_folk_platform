from os import name
from django.contrib.auth.views import LogoutView as logout
from django.urls import path, re_path, include
from django.views.generic.base import RedirectView
from django.contrib.staticfiles.storage import staticfiles_storage
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('chats/', views.chats, name='chats'),
    path('chat/<int:chat_id>/', views.message_view, name='chat'),
    path('ajax/nightmode/', views.nightmode, name='nightmode'),
    path('ajax/createchat/', views.createchat, name="createchat"),
    path('ajax/search_contacts/', views.search_contacts, name="search_contacts"),
    path('ajax/send_message/', views.send_message, name='send_message'),
    path('ajax/invite_user/', views.invite_user, name="invite_user"),
    path('ajax/send_comment/', views.add_comment, name="add_comment"),
    path('ajax/random_chat/', views.randomchat, name="randomchat"),
    path('logout/', logout.as_view(next_page='index'), {'next_page': 'index'}, name='logout'),
    path('register/', views.register_view, name='register'),
    path('profile/', views.profile, name='profile'),
    path('contacts/', views.contacts, name="contacts"),
    path('comments/', views.comments, name="comments"),
    path('favicon.ico', RedirectView.as_view(url=staticfiles_storage.url('img/favicon.ico'))),
    path('oauth/', include('social_django.urls', namespace='social')),
    path('join/<str:link>/', views.join, name="join"),
]