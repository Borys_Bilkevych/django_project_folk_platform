from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db import models
from django.conf import settings
import string, random


class AccountManager(BaseUserManager):
    def create_user(self, email=None, username=None, password=None):
        # if not email:
        #     raise ValueError("Users must have an email adress")
        if not username:
            raise ValueError("Users must have a username")
        # if not password:
        #     raise ValueError("Users must have a password")

        user = self.model(
            email = self.normalize_email(email),
            username=username,
        )

        if password:
            user.set_password(password)
        user.save(using=self._db)
        return user
    
    def create_superuser(self, email, username, password):
        if not email:
            raise ValueError("Users must have an email adress")
        if not username:
            raise ValueError("Users must have a username")
        if not password:
            raise ValueError("Users must have a password")

        user = self.model(
            email = self.normalize_email(email),
            username=username,
            password=password,
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

class Chat(models.Model):
    name = models.CharField(max_length=40, unique=False, default="chat")
    keyword = models.CharField(max_length=200, unique=False, default="Flowers")
    members = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name="members")
    messages_count = models.IntegerField(default=0)
    created = models.DateField(auto_now_add=True)
    last_active = models.DateField(auto_now_add=True)
    creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="creator")
    private = models.BooleanField(default=True, verbose_name="private")

    def __str__(self):
        return f"Chat({self.name}<{self.private}>)"
    
    def add_message(self):
        self.messages_count+=1
        return self.messages_count

class User(AbstractBaseUser):
    email = models.EmailField(verbose_name="email", max_length=60, unique=False)
    username = models.CharField(max_length=30, unique=True)
    password = models.CharField(max_length=30)
    nightmode = models.BooleanField(verbose_name="night_mode", default=True)
    messages_count = models.IntegerField(default=0)
    is_guest = models.BooleanField(verbose_name="guest", default=False)
    avatar = models.ImageField(verbose_name="avatar")
    date_joined = models.DateTimeField(verbose_name="date joined", auto_now_add=True)
    last_login = models.DateTimeField(verbose_name="last login", auto_now_add=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = ["email", "password"]

    objects = AccountManager()

    def __str__(self):
        return self.username
    
    def has_perm(self, perm, obj=None):
        return self.is_admin
    
    def has_module_perms(self, app_label):
        return True
    
    def add_message(self):
        self.messages_count+=1
        return self.messages_count

class Message(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    chat = models.ForeignKey(to=Chat, on_delete=models.CASCADE)
    message = models.CharField(max_length=1200)
    timestamp = models.DateTimeField(auto_now_add=True)
    is_read = models.BooleanField(default=False)

    def __str__(self):
        return self.message

    class Meta:
        ordering = ('timestamp',)

class Invite(models.Model):
    url = models.CharField(max_length=20, unique=True)
    chat = models.ForeignKey(to=Chat, on_delete=models.CASCADE)
    creator = models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    used = models.IntegerField(default=0)

    def use(self):
        self.used+=1
        return self.used
    
    def generate_url(self):
        url = ""
        for _ in range(10):
            url+=random.choice(string.ascii_letters)
        return url

class Comment(models.Model):
    message = models.CharField(max_length=1000)
    author = models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

class Guest_Count(models.Model):
    count = models.IntegerField(default=0)

    def __str__(self):
        return f"Guest_Count({self.count})"
    
    def add(self):
        self.count += 1
        return self.count

    @property
    def c(self):
        return self.count
