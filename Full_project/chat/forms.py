from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.forms import TextInput
from django.contrib.auth import get_user_model
User = get_user_model()


class SignUpForm(UserCreationForm):
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control'}))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    class Meta:
        model = User
        fields=['username','email','password1','password2']
        widgets = {
            'username': TextInput(attrs={'class':'form-control'}),
        }