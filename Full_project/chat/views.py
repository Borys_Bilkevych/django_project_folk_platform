import re, random, datetime
from django import forms
from . import forms as _forms
from django.contrib.auth import authenticate, login
from django.http.response import Http404, HttpResponseBadRequest, JsonResponse, HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from chat.models import Message
from chat.forms import SignUpForm
from chat.serializers import MessageSerializer, UserSerializer
from django.core import serializers
from . import models
from django.contrib.auth import get_user_model

User = get_user_model()
from django.shortcuts import render
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin

class HomeView(LoginRequiredMixin, TemplateView):
    template_name = "chat.html" 

def index(request):
    if request.user.is_authenticated:
        return redirect('chats')
    if request.method == 'GET':
        return render(request, 'chat/index.html', {})
    if request.method == "POST":
        print(request.POST)
        if "guest" in request.POST:
            if len(models.Guest_Count.objects.all()):
                counter =  models.Guest_Count.objects.all()[0]
            else:
                counter = models.Guest_Count()
            count = counter.add()
            counter.save()
            guest = User(username=f"Guest{count}", password="password", nightmode=True, email="guest@email", is_guest=True)
            guest.save()
            login(request, guest, backend='django.contrib.auth.backends.ModelBackend')
            return redirect('chats')
        if not len(request.POST["username"]) or not len(request.POST["password"]):
            return redirect('index')
        username, password = request.POST['username'], request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
        else:
            return HttpResponse('{"error": "User does not exist"}')
        return redirect('chats')



@csrf_exempt
def message_list(request, sender=None, receiver=None):
    """
    List all required messages, or create a new message.
    """
    if request.method == 'GET':
        messages = Message.objects.filter(sender_id=sender, receiver_id=receiver, is_read=False)
        serializer = MessageSerializer(messages, many=True, context={'request': request})
        for message in messages:
            message.is_read = True
            message.save()
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = MessageSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


def register_view(request):
    """
    Render registration template
    """
    if request.method == 'POST':
        # print("working1")
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user.set_password(password)
            user.save()
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user, backend='django.contrib.auth.backends.ModelBackend')
                    return redirect('chats')
    else:
        # print("working2")
        form = SignUpForm()
    template = 'chat/register.html'
    context = {'form':form}
    return render(request, template, context)

@csrf_exempt
def chats(request):
    if not request.user.is_authenticated:
        return redirect('index')
    if request.method == "GET":
        user = request.user
        public_chats = models.Chat.objects.filter(private=False)
        private_chats = []
        for chat in models.Chat.objects.filter(private=True):
            if user in chat.members.all(): private_chats.append(chat)
        return render(request, 'chat/chat.html',
                      {'user': user, "private_chats": private_chats, "public_chats":public_chats})

@csrf_exempt
def send_message(request):
    if request.method == "POST":
        chat = models.Chat.objects.filter(id=request.POST['chat_id'])[0]
        chat.add_message()
        chat.save()
        request.user.add_message()
        request.user.save()
        message = models.Message(author=request.user, message=request.POST['message'], is_read=False, chat=chat)
        message.save()
        return JsonResponse({"author_name":request.user.username, "message":request.POST['message']})

@csrf_exempt
def add_comment(request):
    if request.user.is_guest:
        return JsonResponse({"guest":request.user.username})
    comment = models.Comment(message=request.POST["comment"], author=request.user)
    comment.save()
    return JsonResponse({"message":request.POST['comment'], "author":request.user.username})

def nightmode(request):
    if request.method == "GET":
        mode = request.GET["nightmode"]
        # print(mode)
        mode = not (mode == "True") and not (mode == "true")
        user = User.objects.filter(username=request.user.username)[0]
        user.nightmode = mode
        user.save()
        # print(mode)
        data = {"nightmode": mode}
        return JsonResponse(data)

def randomchat(request):
    public_chats = models.Chat.objects.filter(private=False)
    private_chats = []
    for chat in models.Chat.objects.filter(private=True):
        if request.user in chat.members.all(): private_chats.append(chat)
    chats = list(public_chats)+private_chats
    if not len(chats):
        return redirect('index')
    chat = random.choice(chats)
    return JsonResponse({"url":f"{request.scheme}://{request.get_host()}/chat/{chat.id}/"})

@csrf_exempt
def createchat(request):
    if not request.user.is_authenticated:
        return redirect('index')
    if request.method == "POST":
        data = request.POST
        # print(data)
        chat = models.Chat(name=data["chatname"], creator=request.user, private=(data["state"]=="private"))
        chat.save()
        chat.members.add(request.user)
        # print(chat)
        return JsonResponse({"url":f"{request.scheme}://{request.get_host()}/chat/{chat.id}/"})

def message_view(request, chat_id):
    if not request.user.is_authenticated:
        return redirect('index')
    chat = models.Chat.objects.filter(id=chat_id)[0]
    members = chat.members.all() if chat.private else User.objects.filter(is_guest=False)
    #if request.user not in members:
        #return redirect('join')
    messages = models.Message.objects.filter(chat=chat)
    return render(request, "chat/messages.html",
                      {'messages': messages, "chat":chat, "members":members})

def profile(request):
    if not request.user.is_authenticated:
        return redirect('index')
    chats = models.Chat.objects
    popular = chats.order_by("-messages_count")
    unpopular = popular.reverse()
    pub_chats_count = len(models.Chat.objects.filter(private=False))
    priv_chats_count = len(models.Chat.objects.filter(private=True))
    chats_count_user = len(models.Chat.objects.filter(creator=request.user))
    messages_count = len(models.Message.objects.filter(author=request.user))
    allmessages = len(models.Message.objects.filter())
    status_dict = {100:'Begginer',500:'Standard',2500:'Alcoholic', 10000:'Crazy man', 15000:'Flooder'}
    status_num = {'Begginer':1,'Standard':2,'Alcoholic':3,'Crazy man':4,'Flooder':5}
    users = User.objects.filter(is_guest=False).order_by("-messages_count")[:5]
    a_users = len(User.objects.filter(is_guest=True))
    now = datetime.datetime.now()
    chats_today = len(models.Chat.objects.filter(created__year=now.year, created__month=now.month, created__day=now.day, creator=request.user))
    for item in status_dict:
        if messages_count > item and item != 15000:
            continue
        status = status_dict[item] if messages_count <= item else "Flooder"
        break
    return render(request, 'chat/profile.html', {"popular_chats":popular[:5],"user_status":[status, status_num[status]],
     "unpopular_chats":unpopular[:5], "private_count":priv_chats_count, "public_count":pub_chats_count, "messages":messages_count, "users":users, "a_users":a_users, "chats_today":chats_today, "chats_count_user":chats_count_user, "allmessages": allmessages})

def group(request):
    return render(request, "chat/group.html")

def contacts(request):
    if not request.user.is_authenticated:
        return redirect('index')
    return render(request, "chat/contacts.html",
            {   "user":request.user,
                "users":User.objects.exclude(username=request.user.username).exclude(is_guest=True),
                })
    
@csrf_exempt
def search_contacts(request):
    if request.method == "GET":
        print(request.GET)
        users = User.objects.exclude(is_guest=True).exclude(username=request.user.username)
        term = request.GET['term'].lower()
        resp = []
        for item in users:
            if term in item.username.lower():
                print(term, item.username)
                resp.append({"name":item.username, "email":item.email})
        print(resp)
        return JsonResponse({"users":resp}) # {"users":[{"name":name1,"email":email1}]}

def invite_user(request):
    if request.method == "GET":
        invite = models.Invite(chat=models.Chat.objects.filter(id=request.GET["chat_id"])[0], creator=request.user)
        url = invite.generate_url()
        while len(models.Invite.objects.filter(url=url)):
            url = invite.generate_url()
        invite.url = url
        invite.save()
        return JsonResponse({"url":url, "host":request.scheme+"://"+request.get_host()})

def comments(request):
    if not request.user.is_authenticated:
        return redirect('index')
    return render(request, "chat/comments.html", {"comments":models.Comment.objects.all()})

def join(request, link):
    if not request.user.is_authenticated:
        return redirect('index')
    objects = models.Invite.objects.filter(url=link)
    if not len(objects):
        raise Http404
    invite = objects[0]
    if request.user in invite.chat.members.all():
        return redirect("chat", chat_id=invite.chat.id)
    invite.chat.members.add(request.user)
    invite.use()
    invite.save()
    return redirect("chat", chat_id=invite.chat.id)

def test(request):
    return render(request, "chat/test.html")