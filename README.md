<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gl.vlabs.knu.ua/frecs/ce/techpractice/2020-2021/chat/django-project-bazar-platform">
    <img src="logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">PivChat-README</h3>

  <p align="center">
    An awesome README for PivChat Django Project!
    <br>
    <a href="https://gl.vlabs.knu.ua/frecs/ce/techpractice/2020-2021/chat/django-project-bazar-platform"><strong>Explore the docs »</strong></a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
    <li><a href="#compliance-with-task">Compliance with task</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->
## About The Project

Information rearding to TZ:

* https://docs.google.com/presentation/d/17x0J96_yVKlafOQdBzForIxGJSskAjf-5uce4GzfpWI/edit?usp=sharing

A list of commonly used resources that I find helpful are listed in the acknowledgements.

### Built With

* [Bootstrap](https://getbootstrap.com)
* [JQuery](https://jquery.com)
* [JavaScript](https://javascript.com)
* [CSS](https://css.com)
* [Python](https://python.com)
* [Django](https://django.com)


<!-- GETTING STARTED -->
## Getting Started


### Prerequisites
1. Install Python [https://www.python.org/downloads/](https://www.python.org/downloads/)

2. Install Django [https://www.djangoproject.com/download/](https://www.djangoproject.com/download/)

3. Install Git [https://git-scm.com/downloads](https://git-scm.com/downloads)

4. Install Docker [https://docs.docker.com/docker-for-windows/install/](https://docs.docker.com/docker-for-windows/install/)

5. Follow this instruction for Docker installation:
[https://docs.microsoft.com/ru-ru/windows/wsl/install-win10](https://docs.microsoft.com/ru-ru/windows/wsl/install-win10)

### Installation

1. Clone the repo
   ```sh
   git clone https://gl.vlabs.knu.ua/frecs/ce/techpractice/2020-2021/chat/django-project-bazar-platform.git
   ```
2. Open VSCode and write bellow command in terminal
   ```sh
   docker-compose up
   ```
3. Open two CMD (in project path)
   ```sh
    docker run -p 6379:6379 -d redis   
   ```
   ```sh
   python manage.py runserver 127.0.0.1:8080
   ```
4. If you get a problem with non installed modules. Find relevant command in Google. It's usually looks like
   ```sh
   pip install <module_name>
   ```
5. Start in Relevant Hoising (using pgrok)  
   ```sh
   pgrok64.exe -serveraddr grok.vlabs.knu.ua -authtoken practice2021:255b4e18d2f14f77951f1c89d7636091 -subdomain PivChat 8080
   ```
6. Open [https://pivchat.grok.vlabs.knu.ua/](https://pivchat.grok.vlabs.knu.ua/)
<!-- CONTACT -->
## Contact

* Borys Bilkevych - [@Borys] - borysbilkevych@gmail.com
* Gosha Tatarchenko - [@gankanbi4] - goshan200083@gmail.com
* Max Kho*** - [@max_xkh] - email@example.com

Project Link: [https://gl.vlabs.knu.ua/frecs/ce/techpractice/2020-2021/chat/django-project-bazar-platform](https://gl.vlabs.knu.ua/frecs/ce/techpractice/2020-2021/chat/django-project-bazar-platform)



<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* [GitHub Emoji Cheat Sheet](https://www.webpagefx.com/tools/emoji-cheat-sheet)
* [Img Shields](https://shields.io)
* [Choose an Open Source License](https://choosealicense.com)
* [GitHub Pages](https://pages.github.com)
* [Animate.css](https://daneden.github.io/animate.css)
* [Loaders.css](https://connoratherton.com/loaders)
* [Slick Carousel](https://kenwheeler.github.io/slick)
* [Smooth Scroll](https://github.com/cferdinandi/smooth-scroll)
* [Sticky Kit](http://leafo.net/sticky-kit)
* [JVectorMap](http://jvectormap.com)
* [Font Awesome](https://fontawesome.com)


<!-- COMPLIANCE WITH TASK-->
### Compliance with task
* All compliance with paragraphs, what hasn't been done and what has been done additionally you can find by this link:
* https://docs.google.com/presentation/d/17x0J96_yVKlafOQdBzForIxGJSskAjf-5uce4GzfpWI/edit?usp=sharing
